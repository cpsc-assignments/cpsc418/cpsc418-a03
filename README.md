# CPSC 418 - Secure File Transfer with Prior Key Agreement

**Author**: Artem Golovin
**UID**: 30018900
**Email**: artem.golovin@ucalgary.ca

## Files Submitted

```
# Client / Server

── Client.java        - Contains logic to connect to the specified server.
                        Client will need to provide a shared key, file source, and it's destination name.
── Server.java        - Contains logic to start the server and accept incoming connections.
                        Server will need to enter the key, that will be used by clients to connect and perform file transfer.
── ServerThread.java  - A server thread object, that performs exchange between Client and Server.
                        File integrity is verefied here.
── SenderReceiver.java  - Helper class, used to remove redundant methods in Server/ServerThread and Client

# Crypto helpers

── Decryption.java    - Singleton class that is responsible for decryption process.
── Encryption.java    - Singleton class, that is responsible for encryption process.
── Constants.java     - Used to store constants.

# Utilities

── CryptoUtils.java   - Contains helper methods related to encryption/decryption processes.
── Utils.java         - Contains generic utility methods used throughout the code.
── Log.java           - Console output helper class. Also responsible for printing debug messages
                         if debug flag is passed, when starting the server.
── Console.java       - Helper class to simplify prompts.
── PrimeUtils.java    - Used to generate safe primes, calculate primitive root and generate random exponents
```

## Program Description

### Server

When Server is started, a safe prime p is calculated as follows:

1. Server generates a 511 bit random prime number q
2. Checks whether p = 2q + 1 is prime
3. If it's not, goes back to step 1.

Then, Server calculates primitive root g modulo p and generates it's random exponent b. Values g and p are also sent to the Client, in order for Client to calculate the key. Server calculates g^b (mod p) to send it to Client later.

### Client

When Client is started, client accepts g and p sent by server and generates its own random exponent a. Then, Client calculates g^a (mod p) and sends it to the Server. Server calculates g^b (mod p) and sends it to the Client. Both parties now are able to generate g^ab (mod p) and use that as key. The key is hashed using SHA-256 algorithm. That key used to encrypt and decrypt data that's being sent.

User enters file source path and destination name / path. HMAC-SHA-1 of data bytes and destination path is calculated. Then both source file data and destination file name are decrypted using AES-128. Encryption process is the same as it was in Assignemnt 1 and done using Encryption class. After the encryption process, resulting byte arrays are sent to the server and are validated on ServerThread.

Client connection is terminated automatically after the transfer finished and message is printed to the client, notifying either about successful transfer or failed transfer.

### Server Thread

ServerThread class acts as a middle man. Here, data from the client is being received, verified, and upon successful verification, data is written to specified destination. To ensure data integrity, ServerThread decrypts the data that was sent using Decryption class and seed that was specified by the Client as the key. Then HMAC-SHA-1 is calculated to verify the data and compared to the hash that was sent by the Client. If both data and source have been delivered correctly, only then the file is written to specified destination.

If file was written correctly, ServerThread sends the Client `true` so that Client is notified about successful file transfer. Otherwise, flag `false` is send.

### Encryption Process

File that is passed by used is encrypted using AES-128 encryption algorithm with CBC as the mode of operation for encryption. The following steps are performed during the encryption process:

1. File is read to byte array and encoded using Base64 encoding.
1. SHA-256 Message Digest of original file is calculated.
2. Message digest is appended to the file that will be encrypted.
3. During the encryption process, a random key based on seed, that is passed by user, generated using `SHA1PRNG`.
4. In order to encrypt the file using CBC, an array of 16 randomly generated bytes is added to the contents of encrypted file.

### Decryption Process

To decrypt the file, the following is done:

1. IV is separated from the encrypted data.
2. File is decrypted using AES-128 with CBC mode for decryption.
3. If IV is valid, decryption process will not fail.
4. File bytes and digest are separated from decrypted data.
    - Since message digest is 256 bits long (32 bytes), the content of decrypted digest copied from array of decrypted bytes
5. Then, program calculates hash of decrypted file and compares it to appended message digest.
6. If they are equal, the file is successfully decrypted and written to specified file

## How to run

```bash
make build

# or run this to get detailed information about compilation
# make build_verbose 
```

To start the server, run the following (**Note**: debug flag is optional):

```bash
java Server.java [port] [debug]
```

To connect to the server, run the following:

```bash
java Client.java [address] [port] [debug]
```

## List of what is implemented

Problem is solved in full, according to the problem statement.

## Known Bugs

No known bugs.
