import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class Main {
    public static void main(String[] args) throws InvalidKeyException, NoSuchAlgorithmException {
        Log.out("%d", PrimeUtils.getInstance().getSafePrime());
        Log.out("%d", PrimeUtils.getInstance().getSafePrime());
        Log.out("%d", PrimeUtils.getInstance().getSafePrime());
//        BigInteger p = BigInteger.ONE;
//        BigInteger q = BigInteger.ZERO;
//        BigInteger g = BigInteger.ONE;
//
//        do {
//            q = new BigInteger(Constants.KEY_BIT_LEN - 1, Constants.CERTAINTY_VAL, new Random());
//            p = p.add(q.multiply(new BigInteger("2")));
//        } while (!p.isProbablePrime(Constants.CERTAINTY_VAL));
//
//        BigInteger a = CryptoUtils.getRandExponent(p); // client
//        BigInteger b = CryptoUtils.getRandExponent(p); // server
//
//        Log.out("generated p = %d", p);
//        Log.out("generated q = %d", q);
//        Log.out("generated g = %d", CryptoUtils.primitiveRoot(p));
//        Log.out("rand a = %d", a);
//        Log.out("rand b = %d", b);
//
//        if (a.compareTo(p.subtract(CryptoUtils.TWO)) <= 0 && b.compareTo(p.subtract(CryptoUtils.TWO)) <= 0) {
//            BigInteger gPowA = g.modPow(a, p); // client
//            BigInteger gPowB = g.modPow(b, p); // server
//
//            Log.out("client = %d", gPowA);
//            Log.out("server = %d", gPowB);
//
//            BigInteger gPowAB = gPowA.modPow(b, p); // client
//            BigInteger gPowBA = gPowB.modPow(a, p); // server
//
//            Log.out("client: key = %d", gPowAB);
//            Log.out("server: key = %d", gPowBA);
//            System.out.println(gPowAB.compareTo(gPowBA) == 0);
//
//            System.out.println(Utils.getHexString(
//                    CryptoUtils.HMACSHA1(gPowAB.toByteArray(), "test")
//            ));
//        }
    }
}
