import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class SenderReceiver {
    private DataOutputStream out;
    private DataInputStream in;

    public byte[] receiveData() throws IOException {
        byte[] data;
        int receivedSrcLen = in.readInt();

        if (receivedSrcLen > 0) {
            data = new byte[receivedSrcLen];
            in.readFully(data, 0, data.length);
            return data;
        } else {
            return null;
        }
    }

    public void sendData(byte[] data) throws IOException {
        out.writeInt(data.length);
        out.write(data);
    }

    public DataOutputStream getOut() {
        return out;
    }

    public DataInputStream getIn() {
        return in;
    }

    public void setOut(DataOutputStream out) {
        this.out = out;
    }

    public void setIn(DataInputStream in) {
        this.in = in;
    }

}
