/**
 * @author Artem Golovin
 * @id     30018900
 * @email  artem.golovin@ucalgary.ca
 *
 * @file   Client.java
 */

import java.io.*;
import java.math.BigInteger;
import java.net.*;

/**
 * Client program.  Connects to the server and sends text accross.
 */

public class Client extends SenderReceiver {
    private int port;
    private Socket socket;

    private String destPath;
    private String sourcePath;

    private BufferedReader in;
    private Console console = Console.newInstance();
    private BigInteger a;

    /**
     * Constructor, in this case does everything.
     *
     * @param ip   The hostname to connect to.
     * @param port The port to connect to.
     */
    public Client(String ip, int port) {
        try {
            this.port = port;
            this.socket = new Socket(InetAddress.getByName(ip), port);
            this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            super.setIn(new DataInputStream(socket.getInputStream()));
            super.setOut(new DataOutputStream(socket.getOutputStream()));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Establishes connection to server, whose address and port was passed to constructor.
     * Upon connection, the client is prompted for:
     *   - Shared key
     *   - Path to source file, that will be sent to server
     *   - File destination
     * If shared key, entered by client is correct, the source and destination will be delivered correctly,
     *  otherwise, the server will respond with failure and client will abort with failure message.
     *
     * If file was decrypted and written to specified destination, the client will abort with message notifying user
     *  about successful transfer.
     */
    private void establishConnection() {
        try {
            Log.debug("client: connected to %s:%d", socket.getInetAddress().getHostAddress(), port);

            BigInteger safePrime = new BigInteger(receiveData());
            BigInteger keyGenerator = new BigInteger(receiveData());

            setA(PrimeUtils.getInstance().getRandExponent(safePrime));
            BigInteger clientBase = keyGenerator.modPow(a, safePrime);

            Log.debug("client: safe prime = %d", safePrime);
            Log.debug("client: b = %d", getA());
            Log.debug("client: key generator = %d", keyGenerator);
            Log.debug("client: g^a (mod p) = %d", clientBase);

            BigInteger serverBase = new BigInteger(receiveData());

            Log.debug("client: g^b (mod p) = %d", serverBase);

            sendData(clientBase.toByteArray());

            BigInteger key = serverBase.modPow(getA(), safePrime);

            Log.debug("client: private key = %d", key);
            byte[] hashedKey = CryptoUtils.SHA256Hash(key.toByteArray());

            Log.debug("client: hashed key = %s", Utils.getHexString(hashedKey));

            setSourcePath(console.prompt("src: "));
            setDestPath(console.prompt("dest: "));

            Encryption encryption = Encryption.getInstance().setSeed(hashedKey);
            File src = new File(getSourcePath());
            byte[] srcHash = CryptoUtils.HMACSHA1(src, hashedKey);
            byte[] encryptedSrc = encryption.encrypt(src).getResult();

            byte[] destHash = CryptoUtils.HMACSHA1(src, hashedKey);
            byte[] encryptedDest = encryption.encrypt(getDestPath().getBytes()).getResult();

            sendData(srcHash);
            sendData(encryptedSrc);

            sendData(destHash);
            sendData(encryptedDest);

            boolean success = Boolean.parseBoolean(in.readLine());

            if (success) {
                Log.out("Files were delivered successfully");
                socket.close();
            } else {
                Log.out("Files were not delivered successfully");
                socket.close();
            }

        } catch (IOException e) {
            Log.out("Could not read from input.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDestPath(String destPath) {
        this.destPath = destPath;
    }

    private String getDestPath() {
        return destPath;
    }

    private void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    private String getSourcePath() {
        return sourcePath;
    }

    private BigInteger getA() {
        return a;
    }

    private void setA(BigInteger a) {
        this.a = a;
    }

    /**
     * Main method, starts the client.
     *
     * @param args args[0] needs to be a hostname, args[1] a port number and optional debug flag args[3] .
     */
    public static void main(String[] args) {
        if (args.length > 3) {
            System.out.println("Usage: java Client hostname port# debug");
            System.out.println("hostname is a string identifying your server");
            System.out.println("port is a positive integer identifying the port to connect to the server");
            return;
        }

        try {
            String address = args[0];
            int port = Integer.parseInt(args[1]);
            Log.debug = args.length == 3 && args[2] != null && args[2].equals("debug");

            Client c = new Client(address, port);
            c.establishConnection();
        } catch (NumberFormatException e) {
            System.out.println("Usage: java Client hostname port#");
            System.out.println("Second argument was not a port number");
        }
    }
}
