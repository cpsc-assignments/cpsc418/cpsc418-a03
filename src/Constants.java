/**
 * @author Artem Golovin
 * @id     30018900
 * @email  artem.golovin@ucalgary.ca
 *
 * @file   Constants.java
 */

public class Constants {
    public static final int IV_SIZE = 16;
    public static final int SHA256_HASH_LENGTH = 32;
    public static final int KEY_BIT_LEN = 512;
    public static final int CERTAINTY_VAL = 3;
}
