/**
 * @author Artem Golovin
 * @id     30018900
 * @email  artem.golovin@ucalgary.ca
 *
 * @file   Server.java
 */

import java.io.IOException;
import java.math.BigInteger;
import java.net.*;
import java.util.Vector;

/**
 * Server application for Java socket programming with multithreading.
 * Opens a server socket and listens for clients.  When one connects
 * a thread is spawned to deal with the client.
 */
public class Server {
    private ServerSocket serverSocket;
    private Vector<ServerThread> serverThreads; // holds the active threads
    private boolean shutdown;                   // allows clients to shutdown the server
    private int clientIdCounter;                // id numbers for the clients
    private BigInteger b;
    private BigInteger safePrime;
    private BigInteger keyGenerator;
    private BigInteger serverBase;

    /**
     * Constructor, makes a new server listening on specified port.
     *
     * @param port The port to listen on.
     */
    public Server(int port) {
        this.clientIdCounter = 0;
        this.shutdown = false;
        this.serverThreads = new Vector<>(0, 1);

        try {
            this.serverSocket = new ServerSocket(port);
            /* Output connection info for the server */
            Log.out("Server IP address: %s:%d",  serverSocket.getInetAddress().getHostAddress(), port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Waits for incoming connections and spins off threads to deal with them.
     */
    private void listen() {
        Socket client;
        ServerThread serverThread;

        this.safePrime = PrimeUtils.getInstance().getSafePrime();
        this.keyGenerator = PrimeUtils.getInstance().primitiveRoot(safePrime);
        this.b = PrimeUtils.getInstance().getRandExponent(safePrime);
        this.serverBase = keyGenerator.modPow(getB(), getSafePrime());

        Log.debug("server: safe prime = %d", getSafePrime());
        Log.debug("server: b = %d", getB());
        Log.debug("server: key generator = %d", getKeyGenerator());
        Log.debug("server: g^b (mod p) = %d", this.serverBase);

        // Should only do this when it hasn't been told to shutdown.
        while (!shutdown) {
            // Try to accept an incoming connection.
            try {
                client = serverSocket.accept();
                Log.debug("Client from %s:%d connected",  client.getInetAddress().getHostAddress(), client.getLocalPort());

                serverThread = new ServerThread(client, this, ++clientIdCounter);
                serverThreads.add(serverThread);
                serverThread.run();
            } catch (IOException e) {
                // Server Socket is closed, probably because a client told the server to shutdown
            }
        }
    }

    /**
     * Called by a thread who's client has asked to exit.  Gets rid of the thread.
     *
     * @param serverThread The ServerThread to remove from the vector of active connections.
     */
    public void kill(ServerThread serverThread) {
        Log.out("killing client %d.", serverThread.getID());
        serverThreads.remove(serverThread);
    }

    /**
     * Called by a thread who's client has instructed the server to shutdown.
     */
    public void killAll() {
        try {
            shutdown = true;
            System.out.println("Shutting Down Server.");

            /* For each active thread, close it's socket.  This will cause the thread
             * to stop blocking because of the IO operation, and check the shutdown flag.
             * The thread will then exit itself when it sees shutdown is true.  Then exits. */
            serverThreads.forEach(serverThread -> {
                try {
                    serverThread.stop();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            serverThreads.clear();

            serverSocket.close();
        } catch (IOException e) {
            System.out.println("Could not close server socket.");
        }
    }

    BigInteger getB() {
        return b;
    }

    BigInteger getKeyGenerator() {
        return keyGenerator;
    }

    BigInteger getSafePrime() {
        return safePrime;
    }

    BigInteger getServerBase() {
        return serverBase;
    }

    /**
     * Allows threads to check and see if the server is shutting down.
     *
     * @return True if the server has been told to shutdown.
     */
    boolean getFlag() {
        return shutdown;
    }

    /**
     * Main method
     *
     * @param args First argument should be the port to listen on.
     */
    public static void main(String[] args) {
        if (args.length > 2) {
            System.out.println("Usage: java Server port# debug");
            return;
        }

        try {
            int port = Integer.parseInt(args[0]);
            Log.debug = args.length == 2 && args[1] != null && args[1].equals("debug");

            Server server = new Server(port);
            server.listen();
        } catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
            System.out.println("Usage: java Server port#");
            System.out.println("Second argument is not a port number.");
        }
    }
}
