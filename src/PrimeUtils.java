import java.math.BigInteger;
import java.util.Random;

public class PrimeUtils {
    private static PrimeUtils instance;
    private static BigInteger P;
    private static BigInteger G;
    public static BigInteger TWO = new BigInteger("2");

    public static PrimeUtils getInstance() {
        if (instance == null) {
            instance = new PrimeUtils();
        }

        return instance;
    }

    public BigInteger getSafePrime() {
        if (P == null) {
            P = BigInteger.ONE;
            BigInteger q;
            Random random = new Random();

            do {
                q = new BigInteger(Constants.KEY_BIT_LEN - 1, Constants.CERTAINTY_VAL, random);
                P = P.add(q.multiply(TWO));
            } while (!P.isProbablePrime(Constants.CERTAINTY_VAL));
        }

        return P;
    }

    public BigInteger primitiveRoot(BigInteger p) {
        if (G == null) {
            BigInteger pMinus1 = p.subtract(BigInteger.ONE);
            BigInteger q = pMinus1.divide(TWO);
            G = BigInteger.ONE;

            boolean isPrimitiveRoot = false;


            while (!isPrimitiveRoot) {
                BigInteger exp = G.modPow(q, p);
                G = G.add(BigInteger.ONE);

                isPrimitiveRoot = exp.compareTo(BigInteger.ONE) != 0;
            }
        }

        return G;
    }

    public BigInteger getRandExponent(BigInteger p) {
        BigInteger pMinus2 = p.subtract(PrimeUtils.TWO);
        return new BigInteger(Constants.KEY_BIT_LEN - 1, new Random()).mod(pMinus2);
    }
}
