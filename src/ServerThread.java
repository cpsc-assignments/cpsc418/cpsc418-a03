/**
 * @author Artem Golovin
 * @id     30018900
 * @email  artem.golovin@ucalgary.ca
 *
 * @file   ServerThread.java
 */

import java.math.BigInteger;
import java.net.*;
import java.io.*;

/**
 * Thread to deal with clients who connect to Server.  Put what you want the
 * thread to do in it's run() method.
 */
public class ServerThread extends SenderReceiver implements Runnable {
    private Socket clientSocket; //The clientSocket it communicates with the client on.
    private Server server;       //Reference to Server object for message passing.
    private int clientId;        //The client's id number.
    private PrintWriter out;

    /**
     * Constructor, does the usual stuff.
     *
     * @param clientSocket  Communication Socket.
     * @param port  Reference to server thread.
     * @param id ID Number.
     */
    public ServerThread(Socket clientSocket, Server port, int id) {
        try {
            this.server = port;
            this.clientSocket = clientSocket;
            this.clientId = id;
            this.out = new PrintWriter(clientSocket.getOutputStream());
            super.setIn(new DataInputStream(clientSocket.getInputStream()));
            super.setOut(new DataOutputStream(clientSocket.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This is what the thread does as it executes.  Listens on the clientSocket
     * for incoming data and then echos it to the screen.  A client can also
     * ask to be disconnected with "exit" or to shutdown the server with "die".
     */
    @Override
    public void run() {
        try {

            sendData(server.getSafePrime().toByteArray());
            sendData(server.getKeyGenerator().toByteArray());
            sendData(server.getServerBase().toByteArray());

            BigInteger clientBase = new BigInteger(receiveData());
            BigInteger key = clientBase.modPow(server.getB(), server.getSafePrime());

            Log.debug("server: private key = %d", key);

            byte[] hashedKey = CryptoUtils.SHA256Hash(key.toByteArray());

            Log.debug("server: hashed key = %s", Utils.getHexString(hashedKey));

            Decryption decryption = Decryption.getInstance().setSeed(hashedKey);
            byte[] receivedSrcHash = receiveData();
            byte[] receivedSrc = receiveData();
            byte[] receivedDestHash = receiveData();
            byte[] receivedDest = receiveData();

            byte[] src = decryption.decrypt(receivedSrc).getData();
            String dest = decryption.decrypt(receivedDest).getString();

            boolean srcIsValid = CryptoUtils.validate(receivedSrcHash, src, hashedKey);
            boolean destIsValid = CryptoUtils.validate(receivedDestHash, dest.getBytes(), hashedKey);

            Log.debug("server: valid src received: %s", srcIsValid);
            Log.debug("server: valid dest received: %s", destIsValid);

            if (!srcIsValid && !destIsValid) {
                out.println(false);
                out.flush();
            } else {
                boolean success = Utils.writeDataToPath(src, dest);

                Log.debug("file written: %s", success);

                out.println(success);
                out.flush();
            }

        } catch (UnknownHostException e) {
            System.out.println("Unknown host error.");
        } catch (IOException e) {
            if (server.getFlag()) {
                System.out.println("shutting down.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for id number.
     *
     * @return ID Number
     */
    public int getID() {
        return clientId;
    }

    /**
     * Getter for the clientSocket, this way the server thread can
     * access the clientSocket and close it, causing the thread to
     * stop blocking on IO operations and see that the server's
     * shutdown flag is true and terminate.
     *
     * @return The Socket.
     */
    public Socket getClientSocket() {
        return clientSocket;
    }

    public void stop() throws IOException {
        getClientSocket().close();
    }
}
